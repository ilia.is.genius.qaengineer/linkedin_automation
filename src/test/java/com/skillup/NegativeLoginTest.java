package com.skillup;

import com.skillup.automation.homework4.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


public class NegativeLoginTest extends TestRunner {

    private static final String LOGIN_URL_PAGE = "https://www.linkedin.com/uas/login";
    private static final String EXPECTED_EMAIL_ERROR_MESSAGE = "Please enter an email address or phone number";
    private static final String EXPECTED_PASSWORD_ERROR_MESSAGE = "Please enter a password.";

    private LoginPage loginPage;

    @BeforeMethod
    private void before() {
        loginPage = new LoginPage(driver);
    }


    @Test
    public void verifyNotLoginWithInvalidEmail() {
        driver.get(LOGIN_URL_PAGE);

        loginPage.enterEmail("");
        loginPage.enterPassword("pass3231");
        loginPage.clickOnShowPassLink();
        loginPage.clickOnButton();


        String emailAlertMessageText = loginPage.emailAlertMessage();

        Assert.assertEquals(emailAlertMessageText, EXPECTED_EMAIL_ERROR_MESSAGE, "Email alert message is incorrect!");
    }

    @Test
    public void verifyNotLoginWithInvalidPassword() {
        driver.get(LOGIN_URL_PAGE);

        loginPage.enterEmail("somemail@mail.com");
        loginPage.enterPassword("");
        loginPage.clickOnShowPassLink();
        loginPage.clickOnButton();

        String passwordAlertMessageText = loginPage.passwordAlertMessage();

        Assert.assertEquals(passwordAlertMessageText, EXPECTED_PASSWORD_ERROR_MESSAGE, "Password alert is incorrect!");
    }
}
